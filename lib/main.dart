import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Image _bulidImage(String path, double width, double height) {
  return Image.asset(
    path,
    width: width,
    height: height,
    fit: BoxFit.fitWidth,
  );
}

Row _bulidDetailProfile(String topic, String detail) {
  return Row(
    children: [
      Expanded(
        child: Text(
          topic,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Expanded(
        child: Text(
          detail,
          textAlign: TextAlign.end,
        ),
      )
    ],
  );
}

Container _bulidTopicResume(String text) {
  return Container(
    padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 5.0, bottom: 5.0),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: Colors.blue[800],
    ),
    child: Text(
      text,
      style: TextStyle(fontSize: 21.0, color: Colors.white),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Widget profileSection = Container(
    child: Column(
      children: [
        _bulidImage('images/me.jpg', 200, 300),
        SizedBox(height: 20.0),
        _bulidTopicResume('Profile'),
        SizedBox(height: 20.0),
        _bulidDetailProfile('Name', 'Weerasak Seanglers'),
        _bulidDetailProfile('Nickname', 'Jent'),
        _bulidDetailProfile('Birth of date', '04 June 1999'),
        _bulidDetailProfile('Age', '22'),
        _bulidDetailProfile('Gender', 'Male'),
        SizedBox(height: 20.0),
        _bulidTopicResume('Contact'),
        SizedBox(height: 20.0),
        _bulidDetailProfile('Email', 'weerasak.seanglers@gmail.com'),
        _bulidDetailProfile('Tel', '098-0318526'),
        SizedBox(height: 20.0),
        _bulidTopicResume('Skills'),
      ],
    ),
  );

  Widget skillSection = Container(
    color: Colors.white,
    margin: EdgeInsets.only(left: 8, right: 8),
    height: 160,
    child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              _bulidImage('images/html.png', 70.0, 100.0),
              _bulidImage('images/css.png', 70.0, 100.0),
              _bulidImage('images/js.png', 70.0, 100.0),
              _bulidImage('images/php.png', 70.0, 100.0),
              _bulidImage('images/csharp.png', 70.0, 100.0),
              _bulidImage('images/python.png', 70.0, 100.0),
              _bulidImage('images/java.png', 70.0, 100.0),
              _bulidImage('images/sql.png', 70.0, 100.0),
              _bulidImage('images/vuejs.png', 70.0, 100.0),
              _bulidImage('images/laravel.png', 70.0, 100.0),
            ],
          );
        }),
  );

  Widget educationSection = Container(
    child: Column(
      children: [
        ListTile(
          leading: Icon(Icons.location_city),
          title: Text('High School'),
          subtitle: Text('Junghuasoasieo School'),
          tileColor: Colors.blue[100],
        ),
        ListTile(
          leading: Icon(Icons.location_city),
          title: Text('University'),
          subtitle: Text('Burapha University'),
          tileColor: Colors.blue[100],
        )
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.person),
          title: Text('Resume'),
          backgroundColor: Colors.black,
          leadingWidth: 30,
        ),
        body: Container(
          padding: EdgeInsets.all(32),
          child: ListView(
            shrinkWrap: true,
            children: [profileSection, skillSection, educationSection],
          ),
        ),
      ),
    );
  }
}
